function cl = bwconncomp(h)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
cl = struct;
cl.NumObjects = 0;
cl.PixelIdxList = {};
d = diff(h);
clstart = find(d==1);
clend = find(d==-1);

if h(1) == 1
    clstart = [0 clstart];
end
if h(end) == 1
    clend = [clend length(h)];
end
cl.NumObjects = length(clstart);
for i = 1:cl.NumObjects
    cl.PixelIdxList{i} = (clstart(i)+1):clend(i);    
end



