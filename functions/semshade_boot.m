function semshade_boot(amatrix,alpha,acolor,F,nboot,varargin)
% usage: semshade(amatrix,alpha,acolor,F,nboot,varargin)
% plot mean and 95%-CI coming from a matrix of data, at which each row is an
% observation. 95%-CI is shown as shading.
% - acolor defines the used color (default is red), can be RGB vector
% - alpha defines transparency of the shading, 0 or [] draws lines instead
% of shading
% - F assignes the used x axis (default is steps of 1).
% - nboot number of bootstraps
% varargin forwards parameters for the plot function (e.g. 'LineWidth',2)
%
% modified from smusall 2010/4/23 https://github.com/germanium/figure_tools/blob/master/stdshade.m
%

if exist('acolor','var')==0 || isempty(acolor)
    acolor='r'; 
end

if exist('F','var')==0 || isempty(F); 
    F=1:size(amatrix,2);
end

if exist('lw','var')==0 || isempty(lw); 
    lw = 1;
end

if exist('nboot','var')==0 || isempty(nboot); 
    nboot = 1000;
end
%smth = 1;


if ne(size(F,1),1)
    F=F';
end

amean=nanmean(amatrix);
boot = bootstrp(nboot,@nanmean,amatrix);
sem = std(boot);
ci(1,:) = amean-  sem;
ci(2,:) = amean+  sem;

%astd=1.96*nanstd(amatrix)./sqrt(size(amatrix,1)); % to get std shading
% astd=nanstd(amatrix)/sqrt(size(amatrix,1)); % to get sem shading

if exist('alpha','var')==0 || isempty(alpha) 
    %fill([F fliplr(F)],[amean+astd fliplr(amean-astd)],acolor,'linestyle','none');
    %acolor='k';
    plot([F fliplr(F) F(1)],[ci(2,:) fliplr(ci(1,:)) (ci(2,1))],'Color',acolor); hold on;
    %plot(F,amean-astd,'Color',acolor);
    
else
    fill([F fliplr(F)],[ci(2,:) fliplr(ci(1,:))],acolor, 'FaceAlpha', alpha,'linestyle','none');
end

if ishold==0
    check=true; else check=false;
end

hold on;plot(F,amean,'Color',acolor,varargin{:}); %% change color or linewidth to adjust mean line

if check
    hold off;
end

end

