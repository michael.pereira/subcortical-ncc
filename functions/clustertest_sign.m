function [p,pany,rndpany,idsig] = clustertest_sign(x,baseline,nperm,pthr)
%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This function performs a non-parametric test against a baseline and
% corrects for multiple comparisons across time using a cluster-based
% method.
%
% See Maris, E., and Oostenveld, R. (2007). Nonparametric statistical testing
%   of EEG- and MEG-data. Journal of Neuroscience Methods 164, 177?190. 
%   10.1016/j.jneumeth.2007.03.024.


if nargin < 3
    % number of permutations (default: 1000)
    nperm = 1000;
end
if nargin < 4
    % cluster-level threshold (default: 0.05)
    pthr = 0.05;
end

% default to p-value of 1.
p = 1;

%% uncorrected test
% average over baseline
y = mean(baseline);
n = size(x,1);
% paired tests against baseline
pany_ = nan(1,n);
for k=1:size(x,1)
    var = x(k,:)-y;
    pany_(k) = signrank(var,[],'method','approximate');
end

% get clusters of significance
clmain = bwconncomp(pany_<pthr);

% get length and index of longest cluster
[l,imax] = max(cellfun(@length,clmain.PixelIdxList));

%% permute
% for replicability
rng('default');
fprintf(' [ permutation ] |');
for r=1:nperm
    % progress bar
    if mod(r,100) == 1
        fprintf('.');
    end
    
    % permute signs randomly and test against baseline
    rndpany__ = zeros(1,n);
    sign = round(rand(size(y)))*2-1;
    for k=1:size(x,1)
        var = x(k,:) - y;
        rndpany__(k) = signrank(sign.*var,[],'method','approximate');
    end
    
    % get clusters of significance
    cl = bwconncomp(rndpany__<0.05);
    
    % get length and index of longest cluster
    m = max(cellfun(@length,cl.PixelIdxList));
    
    % assign length, or zero if no cluster
    if cl.NumObjects > 0
        rndpany(r) = m;
    else
        rndpany(r) = 0;
    end
    
end

%% corrected test
if clmain.NumObjects > 0
    pany = l;
    idsig = clmain.PixelIdxList{imax};
    % p-value on permuted null distribution
    p = mean(pany < rndpany);
    fprintf('| cluster (n = %f, p = %.3f)\n',pany,p);
else
    pany = 0;
    idsig = NaN;
    fprintf('| no cluster\n');
end
%[~,~,~,pfdr] = fdr_bh(pany_);

end

