function mybar(varargin)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    c = varargin{1};
    bin = varargin{2};
    c_ = repmat(c(:),[1,2]).';
    bin_ = repmat(bin(:),[1,2]).';
    c_([1]) = [];
    bin_([end]) = [];
    plot(c_(:),bin_(:),varargin{3:end});
end

