%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script plots a raster plot of neuronal firing in rdata over time
% with different colors per condition

function raster(rdata,time,color,sz)
    if nargin < 4 || isempty(sz)
        % default dot size
        sz = 2;
    end
    %%
    u = unique(rdata); u(u==0) = [];
    a = 0;
    for i=u.'
        a = a+1;
       id = find(rdata(:)==u(a)); 
       [y,x] = ind2sub(size(rdata),id);
       plot(time(x),y,'o','Color',color(u(a),:),'MarkerFaceColor',color(u(a),:),'MarkerSize',sz);
       hold on;
    end

end

