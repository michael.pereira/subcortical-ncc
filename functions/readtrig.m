function trig = readtrig(conf)
%READTRIG Summary of this function goes here
%   Detailed explanation goes here

if ~isfield(conf,'subject')
    error('Could not find field *subject* in config');
end
if ~isfield(conf,'analyse_run')
    error('Could not find field *analyse_run* in config');
end

fnameevt = ['../data/' conf.subject '-' num2str(conf.analyse_run) '_evt.tsv'];
trig = readtable(fnameevt,'Delimiter','\t','FileType','text');

% fnamebad = ['data/' conf.subject '-' num2str(conf.analyse_run) '_bad.tsv'];
% 
% if exist(fnamebad,'file') && ~isempty(lim)
%     bad = readtable(fnamebad,'Delimiter','\t','FileType','text');
%     for tr=1:size(trig,1)
%         x = trig.onset(tr);%
%         f = find(bad.tend > x+lim(1) & bad.tstart < x+lim(2));
%         if ~isempty(f)
%             fprintf('Removing trial %d overlap [%.2f,%.2f] [%.2f,%.2f]\n',tr,...
%                 bad.tstart(f(1)),bad.tend(f(1)),x-0.2,x+0.5);
%             trig.trial_type{tr} = 'artefact';
%         end
%         
%     end
% else
%     error('Could not find bad segment file');
% end

%fprintf('==============\nKept %d trials\n============\n',sum(~strcmp(trig.trial_type,'artefact')));
end

