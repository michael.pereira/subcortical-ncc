function [p,st,m1,ci1,m2,ci2] = mysignrank(x,y,txt,prec)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    txt = {'var1','var2'};
end
if nargin < 4
    prec = 2;
end
fprintf('-------------------\n');
[m1,ci1] = describe(x,txt{1},prec);
[m2,ci2] = describe(y,txt{2},prec);
[p,~,st] = signrank(x,y,'method','approximate');
if ~isfield(st,'zval')
    st.zval = NaN;
end
fprintf('ttest: signedrank = %.2f, z = %.2f, p=%f\n',st.signedrank,st.zval,p);

end

