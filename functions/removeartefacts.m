function [behav,bad] = removeartefacts(behav,badseg_file,lim,trigtype)
%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This function checks whether segments marked as an artifact corresponds
% to any of the trials and flags those trials as an artifact
if nargin < 4
    trigtype = 'tonset';
end
if nargin < 3
    lim = [];
end
if nargin < 2
    badseg_file = [];
end

if ~isempty(badseg_file) && ~isempty(lim)
    if exist(badseg_file,'file')
        bad = readtable(badseg_file,'Delimiter','\t','FileType','text');
        for tr=1:length(behav.amp)
            x = behav.(trigtype)(tr);%
            f = find((bad.tend > x+lim(1) & bad.tstart < x+lim(2)));
            if ~isempty(f)
                fprintf('\t\tFlaged trial %d overlap [%.2f,%.2f] [%.2f,%.2f]\n',tr,...
                    bad.tstart(f(1)),bad.tend(f(1)),x+lim(1),x+lim(2));
                behav.type{tr} = 'artifact';
            end
            
        end
    else
        error('Could not file bad segment file');
    end
end
fprintf(' [ artifacts ] Removed %d trials (%d bad segments)\n',sum(strcmp(behav.type,'artifact')),size(bad,1));
end

