function [f, minfval, ffit, osci1,osci2] = gmmfit(y,fsel,nosci,lower,upper,nrep)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
osci1 = [];
osci2 = [];

if nargin < 2 || isempty(fsel)
    fsel = 1:length(y);
end
% numlber of oscillations to fit
if nargin < 3 || isempty(nosci)
    nosci = 2;
end
% lower and upper bounds 
if nargin < 4 || isempty(lower)
    lower = [0,  0, 1,...
        0,  0, 1];
end
if nargin < 5 || isempty(upper)
    upper = [10, 100, 100, ...
        10, 100, 100];
end
% how many repetitions? More adds stability
if nargin < 6 || isempty(nrep)
    nrep = 20;
end
 
if ~isempty(lower)
    lower = lower(1:(nosci*3));
end
if ~isempty(upper)
    upper = upper(1:(nosci*3));
end

minfval = Inf;
for rep=1:nrep
    if mod(rep,1) == 0
        fprintf('.');
    end
    opts = optimoptions('fmincon');
    opts.Display = 'none';
    start = (upper + lower)./2 + (rand(size(upper))*2-1).*(upper - (upper + lower)./2);
 
    if nosci == 1
        [f_,fval] = fmincon(@(x)f1(x,fsel),start,[],[],[],[],lower,upper,[],opts);
        [err,ffit_] = f1(f_,fsel);
    end
    if nosci == 2
        [f_,fval] = fmincon(@(x)f2(x,fsel),start,[],[],[],[],lower,upper,[],opts);
        [err,ffit_] = f2(f_,fsel);
    end
    
    savegof(rep) = fval;
    if fval < minfval
        minfval = fval;
        f = f_;
        ffit = ffit_;
    end
end
fprintf('\n');

%%




i = 0;
if length(f) > i    
    osci1.amp = f(i+1);
    osci1.freq = f(i+2);
    osci1.w = f(i+3);
    
    fprintf('oscillation 1: @%.2f Hz\t | amp: %.2f | width: %.2f\n',...
        osci1.freq,...osci2.freq_ci,...
        osci1.amp,...osci2.amp_ci,...
        osci1.w);%,osci2.w_ci);
    
    i = i + 3;
end
if length(f) > i    
    osci2.amp = f(i+1);
    osci2.freq = f(i+2);
    osci2.w = f(i+3);
    
      fprintf('oscillation 2: @%.2f Hz\t| amp: %.2f | width: %.2f\n',...
        osci2.freq,...osci2.freq_ci,...
        osci2.amp,...osci2.amp_ci,...
        osci2.w);%,osci2.w_ci);
    
end


    function [err,sim] = f1(a,fs)
        % error function for two oscillations 
        sim = nan(size(y));
        % evaluate spectrum
        if isscalar(fs)
            sim = a(1)*normpdf(fs,a(2),a(3))./normpdf(0,0,a(3));
            err = NaN;
            return
        else
            for xi = fs
                sim(xi) = a(1)*normpdf(xi,a(2),a(3))./normpdf(0,0,a(3));
            end
        end
        
        err = nansum(nansum(((y(fs) - sim(fs)).^2)));
        % deal with errors
        if isinf(err), err = 1e100; end
        if isnan(err), err = 1e100; end
    end

    function [err,sim] = f2(a,fs)
        % error function for two oscillations 
        sim = nan(size(y));
        % evaluate spectrum
        if isscalar(fs)
            sim = a(1)*normpdf(fs,a(2),a(3))./normpdf(0,0,a(3)) + ...
                a(4)*normpdf(fs,a(5),a(6))./normpdf(0,0,a(6));
            err = NaN;
            return
        else
            for xi = fs
                sim(xi) = a(1)*normpdf(xi,a(2),a(3))./normpdf(0,0,a(3)) + ...
                    a(4)*normpdf(xi,a(5),a(6))./normpdf(0,0,a(6));
            end
        end
        
        err = nansum(nansum(((y(fs) - sim(fs)).^2)));
        % deal with errors
        if isinf(err), err = 1e100; end
        if isnan(err), err = 1e100; end
    end


end