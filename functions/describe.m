function [m,cint] = describe(x,txt,prec)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 2
    txt = 'var';
end
if nargin < 3
    prec = 2;
end
se = @(x) nanstd(x)./sqrt(length(x));

m = nanmean(x);
cint = se(x);
pr = num2str(prec);
warn = '';
if any(isnan(x))
    warn = sprintf('(%d nans detected)',sum(isnan(x)));
end
fprintf(['%s = %.' pr 'f (se = %.' pr 'f' warn ')\n'],txt,m,cint);
end

