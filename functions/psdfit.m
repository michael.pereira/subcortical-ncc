function [f, minfval, ffit, ffitnoise, noise,theta,beta,gamma] = psdfit(y,fmax,nosci,start,lower,upper,nrep,pen)
%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================


% this function fits an exponential + some oscillatory bumps to a spectrum
% inspired from:
% 
% Martin, S., Iturrate, I., Chavarriaga, R., Leeb, R., Sobolewski, A., 
%   Li, A.M., Zaldivar, J., Peciu-Florianu, I., Pralong, E., Castro-Jim�nez, 
%   M., et al. (2018). Differential contributions of subthalamic beta rhythms 
%   and 1/f broadband activity to motor symptoms in Parkinson?s disease. 
%   npj Parkinson?s Disease 4, 32. 10.1038/s41531-018-0068-y.
%
% Other relevant work: 
%
% Donoghue, T., Haller, M., Peterson, E.J., Varma, P., Sebastian, P., Gao, 
%   R., Noto, T., Lara, A.H., Wallis, J.D., Knight, R.T., et al. (2020). 
%   Parameterizing neural power spectra into periodic and aperiodic components. 
%   Nat Neurosci 23, 1655?1665. 10.1038/s41593-020-00744-x.
%
%

theta = [];
beta = [];
gamma = [];
if nargin < 2 || isempty(fmax)
    fmax = 60;
end
if nargin < 3 || isempty(nosci)
    nosci = 1;
end
if nargin < 4 || isempty(start)
    start =  [0,10,1,0,0,20,1,0,5,1,0,45,1];
end
if nargin < 5 || isempty(lower)
    lower = [-200,    0,   0.01, -10, ...
        0,  8, 2,...
        0,  2, 1, ...
        0,  30,1];
end
if nargin < 6 || isempty(upper)
    upper =[200,    20,  2, 10, ...
        10, 35, 7, ...
        10, 9, 10,...
        10, 70, 20];
end
if nargin < 7 || isempty(nrep)
    nrep = 100;
end
if nargin < 8 || isempty(pen)
    pen = 0;
end
start = start(1:(4+nosci*3));
if ~isempty(lower)
lower = lower(1:(4+nosci*3));
end
if ~isempty(upper)
upper = upper(1:(4+nosci*3));
end

minfval = Inf;
for rep=1:nrep
    if mod(rep,1) == 0
        fprintf('.');
    end
   
    opts = optimoptions('fmincon');
    %opts.Display = 'iter';
    opts.Display = 'none';
    start = (upper + lower)./2 + (rand(size(upper))*2-1).*(upper - (upper + lower)./2);
    if nosci == 0  
        [f_,fval] = fmincon(@f0,start,[],[],[],[],lower,upper,[],opts);
        [~,ffit_] = f0(f_);
    end
    if nosci == 1
        [f_,fval] = fmincon(@f1,start,[],[],[],[],lower,upper,[],opts);
        [~,ffit_] = f1(f_);
    end
    if nosci == 2
        [f_,fval] = fmincon(@f2,star,[],[],[],[],lower,upper,[],opts);
        [~,ffit_] = f2(f_);
    end
    
    savegof(rep) = fval;
    if fval < minfval
        minfval = fval;
        f = f_;
        ffit = ffit_;
    end
end
fprintf('\n');
%%
%fci = confint(f);
fval = f;%coeffvalues(f);
[~,ffitnoise] = f0(fval(1:4));
noise.offset = fval(1);
%noise.offset_ci = fci(:,1);
noise.rot = fval(2);
%noise.rot_ci = fci(:,2);
noise.decay = fval(3);
%noise.decay_ci = fci(:,3);
noise.lin = fval(4);
%noise.lin_ci = fci(:,4);

fprintf('noise: rot: %.2f Hz \t| offset: %.2f  \t| decay: %.2f \t| linear: %.2f \n',...
    noise.rot,noise.offset,noise.decay,noise.lin);

i = 4;
if length(fval) > i
    
    beta.amp = fval(i+1);
    %beta.amp_ci = fci(:,i+1);
    beta.freq = fval(i+2);
    %beta.freq_ci = fci(:,i+2);
    beta.w = fval(i+3);
    %beta.w_ci = fci(:,i+3);
    if beta.amp > 2 %&& beta.freq_ci(1) > 10 && beta.freq_ci(2) < 35
        beta.select = 1;
        txt = ' * ';
    else
        beta.select = 0;
        txt = '   ';
    end
    fprintf('%soscillation 2: @%.2f Hz\t| amp: %.2f\t| width: %.2f\n',...
        txt,beta.freq,...beta.freq_ci,...
        beta.amp,...beta.amp_ci,...
        beta.w);%,beta.w_ci);
    
    i = i + 3;
end
if length(fval) > i
    theta.amp = fval(i+1);
    %theta.amp_ci = fci(:,i+1);
    theta.freq = fval(i+2);
    %theta.freq_ci = fci(:,i+2);
    theta.w = fval(i+3);
    %theta.w_ci = fci(:,i+3);
    
    if theta.amp > 2%_ci(1) > 0 %&& theta.freq_ci(1) >= 2 && theta.freq_ci(2) < 9
        theta.select = 1;
        txt = ' * ';
    else
        theta.select = 0;
        txt = '   ';
    end
    fprintf('%soscillation 1: @%.2f Hz   \t| amp: %.2f  \t| width: %.2f \n',...
        txt,theta.freq,...theta.freq_ci,...
        theta.amp,...theta.amp_ci,...
        theta.w);%,theta.w_ci);
    
end
i = i + 3;
if length(fval) > i
    
    gamma.amp = fval(i+1);
    %gamma.amp_ci = fci(:,i+1);
    gamma.freq = fval(i+2);
    %gamma.freq_ci = fci(:,i+2);
    gamma.w = fval(i+3);
    %gamma.w_ci = fci(:,i+3);
    if gamma.amp > 2 %&& gamma.freq_ci(1) > 30 && gamma.freq_ci(2) < 60
        gamma.select = 1;
        txt = ' * ';
    else
        gamma.select = 0;
        txt = '   ';
    end
    fprintf('%soscillation 3: @%.2f Hz (%.2f - %.2f)\t| amp: %.2f (%.2f - %.2f)\t| width: %.2f (%.2f - %.2f)\n',...
        txt,gamma.freq,...gamma.freq_ci,...
        gamma.amp,...gamma.amp_ci,...
        gamma.w);%,gamma.w_ci);
end

    function [err,sim] = f0(a)
        sim = nan(size(y));
        for xi = 1:1:fmax
            sim(xi) = a(1)+a(2)./(xi.^a(3)) + a(4)*xi;
        end
        err = sum((y(1:fmax) - sim(1:fmax)).^2);
        if isinf(err), err = 1e100; end
        if isnan(err), err = 1e100; end
    end

    function [err,sim] = f1(a)
        sim = nan(size(y));
        for xi = 1:1:fmax
            sim(xi) = a(1)+a(2)./(xi.^a(3)) + a(4)*xi + ...
                a(5)*normpdf(xi,a(6),a(7))./normpdf(0,0,a(7));
        end
        err = sum((y(1:fmax) - sim(1:fmax)).^2) + pen*0.1*a(5)*a(7);
        if isinf(err)
            err = 1e100; 
        end
        if isnan(err)
            err = 1e100; 
        end        
    end

    function [err,sim] = f2(a)
        sim = nan(size(y));
        for xi = 1:1:fmax
            sim(xi) = a(1)+a(2)./(xi.^a(3)) + a(4)*xi + ...
                a(5)*normpdf(xi,a(6),a(7))./normpdf(0,0,a(7)) + ...
                a(8)*normpdf(xi,a(9),a(10))./normpdf(0,0,a(10));
        end
        err = sum((y(1:fmax) - sim(1:fmax)).^2) + pen*(a(5)*a(7) + a(8)*a(10));
        if isinf(err), err = 1e100; end
        if isnan(err), err = 1e100; end        
    end


end