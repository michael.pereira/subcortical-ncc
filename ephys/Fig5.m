anat = 'stn';
tasksel = readtable('processed/cuetask_sel.tsv','FileType','text','Delimiter','\t');
senssel = readtable('processed/sensrev_sel.tsv','FileType','text','Delimiter','\t');
%sensrevsel = readtable('processed/sensrev_sel.tsv','FileType','text','Delimiter','\t');
percsel = readtable('processed/perc_sel.tsv','FileType','text','Delimiter','\t');
units = readtable('processed/units_ext.tsv','FileType','text','Delimiter','\t');

sum(percsel.perc_sel & senssel.sens_sel)
sum(tasksel.task_sel & senssel.sens_sel)

% remove non sua units
tasksel(units.sua==0 | ~strcmp(units.anat,anat),:) = [];
senssel(units.sua==0 | ~strcmp(units.anat,anat),:) = [];
percsel(units.sua==0 | ~strcmp(units.anat,anat),:) = [];
units(units.sua==0 | ~strcmp(units.anat,anat),:) = [];

%
close all
figure(); hold on;
r = 4;
r2 = 0.5;
x = 0; t = -1:0.1:1;
circle = r*exp(1j*pi*t);
box = [r*(t+1i) r*(1+1i*t) r*(-1-1i*t) r*(t-1i) ];
strike = r/sqrt(2)*(t+1i*t);
pcol = [194,77,78]./255;
scol = [113,166,51]./255;
tcol = [97,100,171]./255;

for i=1:size(units,1)
    if strcmp(units(i,:).anat,'stn')
        plot(x+circle,'k','LineWidth',2);
        
    else
        plot(x+box,'k','LineWidth',2);
        
    end
    
    if percsel(i,:).perc_sel == 1
        rnd = 4*rand(1)-2;
        fill(real(x+rnd+r2*circle),imag(x+rnd+r2*circle),'r','FaceColor',pcol,'FaceAlpha',0.3);
    end
    if senssel(i,:).sens_sel == 1
        rnd = 4*rand(1)-2;
        fill(real(x+rnd+r2*circle),imag(x+rnd+r2*circle),'r','FaceColor',scol,'FaceAlpha',0.3);
    end
    if tasksel(i,:).task_sel == 1
        rnd = 4*rand(1)-2;
        fill(real(x+rnd+r2*circle),imag(x+rnd+r2*circle),'r','FaceColor',tcol,'FaceAlpha',0.3);
    end
    
    if percsel(i,:).behav_ok == 0
        plot(x+strike,'k','LineWidth',2);
    end
    
    
    if mod(i,5) == 0
        x = round(i/5)*1i*10;
    else
        x = x + 10;
    end
end

axis equal