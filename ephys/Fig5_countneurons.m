%clear 
addpath ../functions
config_sua
rng('default');
yl = [0,10 ];
ds = load('cntstim.mat');
dp = load('cnt.mat');

figure(); hold on;
plot(ds.tidx_stim_dwn, ds.cntstim,'LineWidth',2,'Color',conf.color.crej);

plot(dp.tidx_dwn, dp.cnt,'LineWidth',2,'Color',conf.color.miss);
xlim([-0.3,0.5])
ylim(yl)

xlabel('Time from stimulus [s]');
ylabel('Neurons with an effect');
plot([0 0],ylim(),'k--');    
set(gcf,'Position',[30,800,200,150]);

[fs0, minfvals0] = gmmfit(ds.cntstim,[],1);
[fs, minfvals, ffits, osci1s,osci2s] = gmmfit(ds.cntstim,[],2);
ts1 = osci1s.freq/100 + min(ds.tidx_stim_dwn);
ts2 = osci2s.freq/100 + min(ds.tidx_stim_dwn);
improves = (minfvals0 - minfvals)./minfvals;
plot(ts1*[1,1],yl,'--','Color',conf.color.crej);
plot(ts2*[1,1],yl,'--','Color',conf.color.crej);

[fp0, minfvalp0] = gmmfit(dp.cnt,[],1);
[fp, minfvalp, ffitp, osci1p,osci2p] = gmmfit(dp.cnt,[],2);
tp1 = osci1p.freq/100 + min(dp.tidx_dwn);
tp2 = osci2p.freq/100 + min(dp.tidx_dwn);
improvep = (minfvalp0 - minfvalp)./minfvalp;

plot(tp1*[1,1],yl,'--','Color',conf.color.miss);
plot(tp2*[1,1],yl,'--','Color',conf.color.miss);

legend({'Sensory','Perception'},'Location','NW')
print(['figs/prod/count.eps'],'-depsc');

fprintf('sensory: t = %.3f s | %.3f s (stdev = %.3f | %.3f, w = %.2f | %.2f (improve: %2f)\n',...
    ts1,ts2,osci1s.w,osci2s.w,osci1s.amp,osci2s.amp,improves);

fprintf('perception: t = %.3f s | %.3f s (stdev = %.3f | %.3f, w = %.2f | %.2f (improve: %2f)\n',...
    tp1,tp2,osci1p.w,osci2p.w,osci1p.amp,osci2p.amp,improvep);

%plot(ds.tidx_stim_dwn, ffits,'--','LineWidth',1,'Color',conf.color.crej);
%plot(dp.tidx_dwn, ffitp,'--','LineWidth',1,'Color',conf.color.miss);