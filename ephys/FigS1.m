%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================


config_sua;

updrs_prepost = [...
    37,13;...
    54,22;...
    24,8;...
    37,24;...
    47,21;...
    31,9;...
    29,19;...
    56,31;...
    56,41;...
    32,12;...
    34,11;...
    43,30;...
    48,29 ...
];

age_pd = [75, 64, 51, 62, 72, 54, 71, 75, 57, 46, 58, 56, 46, 58];
age_et = [60, 64, 79, 54, 59, 70, 70, 70, 87, 76];

describe(age_pd,'Age PD');
describe(age_et,'Age ET');

figure(); hold on;
lat = 0.01*randn(size(updrs_prepost,1),1);
plot(lat,updrs_prepost(:,1),'ko','MarkerFaceColor',conf.color.stn);
plot(1+lat,updrs_prepost(:,2),'ko','MarkerFaceColor',conf.color.stn);
for i=1:size(updrs_prepost,1)
    plot([0,1]+lat(i),updrs_prepost(i,:),'k');
end
xlim([-0.1,1.1])
set(gca,'XTick',0:2,'XTickLabel',{'Pre','Post'});
xlabel('Surgery');
ylabel('UPDRS score');

mysignrank(updrs_prepost(:,1),updrs_prepost(:,2),{'Pre','Post'})

set(gcf,'Position',[155,400,120,250]);