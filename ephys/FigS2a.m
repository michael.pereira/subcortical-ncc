%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script computes statistics for the spike sorting

clear;
addpath ../functions

if ~exist('processed/units_ext.tsv','file')
    fprintf('===================================\n');
    fprintf('=                                 =\n');
    fprintf('= You need to run compute_frate.m =\n');
    fprintf('=                                 =\n');    
    fprintf('===================================\n');
    error('processed/units_ext.tsv not found');
end
config_sua;
sh = 0.01;
cells = readtable('processed/units_ext.tsv','FileType','text','Delimiter','\t');

h1 = figure(); clf;
h2 = figure(); clf;

colors = {conf.color.stn,conf.color.thal};
anat = {'stn','thal'};
for i=1:2
    
cells_ = cells(cells.sua==1 & strcmp(cells.anat,anat{i}),:);

nneu = length(cells_.id);

figure(h1);
subplot(241); hold on;x = xlim();
[hsua,c] = hist(cells_.fire,0:4:40,'support','positive');
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});

xlabel('Mean firing rate [spikes/s]');
ylabel('Count');
disp(sprintf('FR: %.2f�%.2f %%\n',mean(cells_.fire),std(cells_.fire)./sqrt(nneu)));

subplot(242); hold on;x = xlim();
[hsua,c] = hist(cells_.isi*100,-0.25:0.4:3);
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.isi*100,-0.25:0.2:3);
xlabel('% ISI < 3ms');
ylabel('Count');
disp(sprintf('ISI: %.2f�%.2f %%\n',mean(cells_.isi*100),std(cells_.isi*100)./sqrt(nneu)));
%xlim([0,])


subplot(243); hold on;x = xlim();
[hsua,c] = hist(cells_.d,0:5:30,'support','positive');
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist((cells_.d),0:0.5:70);
hold on
%plot([4 4],ylim(),'r--');

xlabel('Proj. dist. [SD]');
ylabel('Count');
disp(sprintf('projD: %.2f�%.2f %%\n',nanmean(cells_.d),nanstd(cells_.d)./sqrt(nneu)));

subplot(244); hold on;x = xlim();
[hsua,c] = hist(cells_.snr,0:1:25,'support','positive');
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.snr,0:0.5:30);
hold on;
%plot([3 3],ylim(),'r--');
xlabel('Peak SNR');
ylabel('Count');
disp(sprintf('SNR: %.2f�%.2f %%\n',mean(cells_.snr),std(cells_.snr)./sqrt(nneu)));

subplot(245); hold on;x = xlim();
[hsua,c] = hist(cells_.cv2,0:0.1:2,'support','positive');
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.cv2,0:0.1:2);
xlabel('Coeff. Var. 2');
ylabel('Count');
disp(sprintf('CV2: %.2f�%.2f %%\n',mean(cells_.cv2),std(cells_.cv2)./sqrt(nneu)));

subplot(246); hold on;x = xlim();
[hsua,c] = hist(cells_.bi,0:0.05:0.5);
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.cv2,0:0.1:2);
xlabel('Burst index');
ylabel('Count');
disp(sprintf('Burst index: %.2f�%.2f %%\n',mean(cells_.bi),std(cells_.bi)./sqrt(nneu)));

subplot(247); hold on;x = xlim();
[hsua,c] = hist(cells_.spikeamp,0:20:400);
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.cv2,0:0.1:2);
xlabel('Spike amp.');
ylabel('Count');
disp(sprintf('Spike amplitude: %.2f�%.2f %%\n',mean(abs(cells_.spikeamp)),std(abs(cells_.spikeamp))./sqrt(nneu)));


subplot(248); 


%%
figure(h2);
cells2_ = cells_(cells_.specbetawidth < 7,:);
subplot(121); hold on;x = xlim();
[hsua,c] = hist(cells2_.specbetafreq,0:2.5:50);
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.cv2,0:0.1:2);
xlabel('Beta frequency');
ylabel('Count');
disp(sprintf('Beta freq: %.2f�%.2f %%\n',mean(cells_.specbetafreq),std(cells_.specbetafreq)./sqrt(nneu)));

subplot(122); hold on;x = xlim();
[hsua,c] = hist(cells2_.specbetaamp,0:0.5:10);
mybar(c+(i==2)*x(2)*sh,hsua,'LineWidth',2,'Color',colors{i});
%hist(cells_.cv2,0:0.1:2);
xlabel('Beta amplitude');
ylabel('Count');
disp(sprintf('Beta amp: %.2f�%.2f %%\n',mean(cells_.specbetaamp),std(cells_.specbetaamp)./sqrt(nneu)));


end
figure(h1)
set(gcf,'Position',[400,600,600,200])