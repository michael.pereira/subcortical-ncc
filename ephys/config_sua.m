%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

%% configuration file

% amount of downsampling for statistics
conf.statdownsample = 10;

% number of permutations for non-parametric tests and boostrapping
conf.randperm = 1000;

% kernel width for computing firing rates
conf.kernelwidth = 0.04;

% min length of significant cluster in time is twice the kernel
conf.sigthr = conf.kernelwidth*2;

% min number of trials per condition to make a non-parametric test
conf.mintrials = 10;

%conf.apidmax = 95;

% sampling frequency at which the spike sorting was performed (do not
% modify)
conf.fs_sua = 30e3;
% sampling frequency at which to compute firing rate
conf.fs_fire = 1000; 
% trial length (do not modify)
conf.trlen = 4;
% epoch timings (do not modify)
conf.trial_stim = [-0.5,1.5];
conf.trial_cue = [-1,4];

conf.clusttype = 'length';
conf.testtype = 'nonparam';

% patients with PD (STN recordings)
conf.allsuj_stn = {'sub-p02','sub-p08','sub-p11','sub-p11','sub-p18','sub-p18', ...
    'sub-p21','sub-p21','sub-p22','sub-p22','sub-p26','sub-p26','sub-p27','sub-p32','sub-p32',...
    'sub-p34','sub-p34','sub-p39','sub-p41','sub-p41','sub-p45','sub-p45',...
    'sub-p46','sub-p60','sub-p64','sub-p64','sub-p66','sub-p67'};
conf.allrun_stn = {1,1,1,2,1,2,...
    1,2,1,2,1,2,1,1,2,...
    1,2,1,1,2,1,2,...
    1,1,1,2,1,1};

% patients with ET (thalamus recordings)
conf.allsuj_thal = {'sub-p03','sub-p06','sub-p06','sub-p15','sub-p31','sub-p31','sub-p37','sub-p38',...
    'sub-p43','sub-p43','sub-p51','sub-p53','sub-p54','sub-p54','sub-p58',...
    'sub-p65','sub-p78','sub-p80','sub-p88','sub-p92','sub-p98','sub-p101'}; % no-neuron: sub-p15 reprocess because of artifacts: sub-p51
conf.allrun_thal = {1,1,2,2,1,2,1,1,...
    1,2,1,1,1,2,1,...
    1,1,1,1,1,1,1};

% always compute for STN and thalamus
conf.allsuj = [conf.allsuj_stn conf.allsuj_thal];
conf.allrun = [conf.allrun_stn conf.allrun_thal];

% colors for plotting
conf.color.hit = [83,169,212]./255;
conf.color.hit1 = [162,202,211]./255;
conf.color.hit2 = [83,169,212]./255;
conf.color.hit3 = [2,61,79]./255;
conf.color.miss = [186,39,50]./255;
conf.color.crej = [149,189,20]./255;
conf.color.fa = [0 0 0]./255;
conf.color.stn = [109,186,150]./255;
conf.color.thal = [224,144,112]./255;
conf.color.short = [146,170,41]./255;
conf.color.long = [205,139,86]./255;
