%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script generates the data and figures for cue- and task-selective
% neurons (Figure 2)

clear;

% make it reproducible
rng('default');

% whether to plot results for each unit
doplot = 1;
% transparency of confidence intervals
alpha = [];

% redo the analysis based on cue OFFSET for reviewer 2?
r2 = 1;
cnt = 0;
nmincatch = 10;
% add function directory
addpath ../functions/

%% General parameters
config_sua;

% time window for data analysis in samples(dont change)
win = -1*conf.fs_fire:4*conf.fs_fire;
win_stim = -0.3*conf.fs_fire:0.5*conf.fs_fire;

% time indices in seconds
tidx = win./conf.fs_fire;
tidx_stim = win_stim./conf.fs_fire;

% time indices in seconds after downsampling
tidx_dwn = tidx(1:conf.statdownsample:end);
tidx_stim_dwn = tidx_stim(1:conf.statdownsample:end);

% min and max time for the plots
xl = [0.3+0.5-0.3, 0.3+0.5+2+0.5];
xl_stim = [-0.3, 0.5];


% min and max time for statistics
statxl = [0, 0.4]; %0.4
statxl_bl = [-0.3, 0]; %0.4

% select time for the clustering algorithm
statsel = find(tidx_stim_dwn >= statxl(1) & tidx_stim_dwn < statxl(2));
statsel_bl = find(tidx_stim_dwn >= statxl_bl(1) & tidx_stim_dwn < statxl_bl(2));

timing = nan(length(statsel),100);

cntstim = zeros(length(tidx_stim_dwn),1);

%% load cell info
cells = readtable('../data/units.tsv','FileType','text','Delimiter','\t');
ncells = size(cells,1);

%% loop through cells
for cell = 1:ncells
    
    unitid = cells(cell,:).id{1};
    conf.subject = num2str(cells(cell,:).id{1}(1:end-6));
    conf.analyse_run = str2double(cells(cell,:).id{1}(end-4));
    fprintf(' -------------------------------------------\n');
    fprintf(' [ task-selective ] processing %s (%s)\n',unitid,cells(cell,:).anat{1});
    
    %% load firing rate
    matfile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_cue.mat'];
    fprintf(' [ task-selective ] loading %s\n',matfile);
    dat = load(matfile,'raster','frate','behav','stat');
    
    % resample so it takes less time to permute
    rate = resample(dat.frate,1,conf.statdownsample);
    
    % remove segments marked as artifacts
    badseg_file = ['../data/' conf.subject '-' num2str(conf.analyse_run) '_bad.tsv'];
    [behav,badtbl] = removeartefacts(dat.behav,badseg_file,xl,'tcue');
    
    % get indices
    bad =  ~(strcmp(behav.type,'perithr') | strcmp(behav.type,'other'));% | behav.amp==0;% | mean(dat.frate(tsel,:))==0 ;
    fprintf(' [ task-selective ] removing %d bad trials\n',sum(bad));
    allbad(cell) = sum(bad);
    % remove bad trials
    behav(bad,:) = [];
    dat.frate(:,bad) = [];
    dat.raster(:,bad) = [];
    rate(:,bad) = [];
    
    % get trial indices
    idxyes = strcmp(behav.resp,'yes');
    idxhit = strcmp(behav.resp,'yes') & behav.amp>0;% & idxperithr;
    idxmiss = strcmp(behav.resp,'no') & behav.amp>0;%& idxperithr;
    idxcrej = strcmp(behav.resp,'no') & behav.amp==0;% & idxperithr;
    idxfa = strcmp(behav.resp,'yes') & behav.amp==0;% & idxperithr;
    idxstim = behav.amp>0;
    idxcatch = behav.amp==0;
    
    ncatch(cell) = sum(idxcatch);
    % save min number of trials
    ntrials(cell) = sum(idxstim);
    
    selstim = find(idxstim);
    %if ncatch(cell) > 10
    stim_rate = nan(81,length(selstim));
    mstim_rate = nan(81,length(selstim));
    zstim_rate = nan(81,length(selstim));
    zstim = nan(1,length(selstim));
    raster_stim = nan(801,length(selstim));
    for tr = 1:length(selstim)
        on = round((1+behav.onset(selstim(tr)))*conf.fs_fire/conf.statdownsample);
        tidx = on+(0:39);
        sti = mean(rate(tidx,selstim(tr)));
        mu = mean(mean(rate(tidx,idxcatch)));
        s = std(mean(rate(tidx,idxcatch)));
        zstim(1,tr) = (sti - mu)/s;
        
        tidx2 = on+(-30:50);
        sti = (rate(tidx2,selstim(tr)));
        mu = mean((rate(tidx2,idxcatch)),2);
        s = std((rate(tidx2,idxcatch)),[],2);
        zstim_rate(:,tr) = (sti - mu);
        stim_rate(:,tr) = sti;
        mstim_rate(:,tr) = mu;
        
        on3 = round((1+behav.onset(selstim(tr)))*conf.fs_fire);
        raster_stim(:,tr) = dat.raster(on3+(-300:500),selstim(tr));
        
    end
    pbl(cell,1) = signrank(zstim);
    
    zstim = zstim_rate(statsel,:);
    zstim_bl = zstim_rate(statsel_bl,:);
    znst = ntrials(cell);
    znct = ncatch(cell);
    save(['processed/stim/' unitid '_zbltrans.mat'],'zstim','zstim_bl','znst','znct');

    % check if sufficient data
    if ntrials(cell) >= 2*conf.mintrials
        isok(cell) = 1;
        % cluster permutation
        [punit(cell),pany(cell),rndpany(cell,:),idsig] = clustertest_sign(zstim_rate(statsel,:),zstim_rate(statsel_bl,:),conf.randperm);
        % x = [stim_rate(statsel,:),mstim_rate(statsel,:)];
        % y = [0*stim_rate(1,:),0*mstim_rate(1,:)+1]==1;
        % [punit(cell),pany(cell),rndpany(cell,:),idsig] = clustertest(x,y,conf.randperm);
        
        if ~isnan(idsig)
            % if there is a significant cluster, record its latency
            timing(statsel(idsig),cell) = mean(zstim_rate(statsel(idsig),:),2);
        end
    else
        % if data insufficient, move on
        isok(cell) = 0;
        punit(cell) = NaN;
        pany(cell) = NaN;
        rndpany(cell,:) = NaN;
        stimampval(cell) = NaN;
        continue
    end
    
    %else
    %    pbl(cell,1) = NaN;
    %end
    
    
    allntrial(:,cell) = [sum(idxhit),sum(idxmiss),sum(idxcrej),sum(idxfa)];
    
    dosave = 0;
    if doplot && (ntrials(cell) > 2*conf.mintrials && ncatch(cell) >= nmincatch)
        %%
        close all
        
        %% firing rate
        cur = figure(100); clf; hold on;
        semshade_boot(stim_rate',alpha,'k',linspace(-0.3,0.5,81),conf.randperm,'LineWidth',2);
        semshade_boot(mstim_rate',alpha,'r',linspace(-0.3,0.5,81),conf.randperm,'LineWidth',2);
        
        % plot significance if any
        ig = nan(1,size(dat.frate,1));
        if ~all(isnan(idsig))
            sig = nan(size(tidx_stim_dwn));
            sig(statsel(idsig)) = mean(mstim_rate(statsel(idsig)));
            plot(tidx_stim_dwn,sig,'k','LineWidth',5)
            if length(statsel(idsig)) >= 8 && cells(cell,:).sua
                dosave = 1;
                cnt = cnt+1;
                cntstim(statsel(idsig)) = cntstim(statsel(idsig)) + 1;
            end
        end
        
        % make it nice
        xlim(xl_stim);
        xlabel('Time from stimulus [s]');
        ylabel('Firing rate (spikes/s)');
        plot([0 0],ylim(),'k--');
        set(gcf,'Position',[50,500,300,400])
        title(unitid);
        set(gcf,'Position',[50,200,150,150])
        print(['figs/sens/sensoryrev_' unitid '_fire.eps'],'-depsc');
        
        cur2 = figure; hold on;
        
        % raster for all trials
        raster(raster_stim',tidx_stim,[0 0 0] ,1);
        
        % make it nice
        xlim(xl_stim);
        ylim([0,size(raster_stim,2)]);
        plot([0 0],ylim(),'k--');
        title(unitid);
        xlabel('Time from stimulus [s]');
        ylabel('Trials');
        set(gcf,'Position',[50,600,150,150])
        saveas(cur2,['figs/sens/sensoryrev_' unitid '_raster.png']);
        
        %%
        % cur4 = figure; hold on;
        % sel = find(tidx_stim_dwn >= xl_stim(1) & tidx_stim_dwn <= xl_stim(2));
        % 
        % % plot firing rate over time for hits and misses
        % ihit = idxhit(selstim);
        % imiss = idxmiss(selstim);
        % 
        % plot(tidx_stim_dwn(sel),mean(stim_rate(sel,ihit),2),'-','Color',conf.color.hit,'LineWidth',1.5);
        % plot(tidx_stim_dwn(sel),mean(stim_rate(sel,imiss),2),'-','Color',conf.color.miss,'LineWidth',1.5);
        % plot(tidx_stim_dwn(sel),mean(stim_rate(sel,:),2),'Color','k','LineWidth',2);
        % 
        % % plot significance if any
        % ig = nan(1,size(dat.frate,1));
        % if ~all(isnan(idsig))
        %     sig = nan(size(tidx_stim_dwn));
        %     sig(statsel(idsig)) = mean(mstim_rate(statsel(idsig)));
        %     plot(tidx_stim_dwn,sig,'k','LineWidth',5)
        % end
        % 
        % % make it nice
        % xlim(xl_stim);
        % set(gca,'XTickLabel',{});
        % plot([0 0],ylim(),'k--');
        % title(unitid);
        % %set(gca,'FontSize',18);
        % set(gcf,'Position',[50,200,120,100])
        % print(['figs/prod/sup_' unitid '_fire2b.eps'],'-depsc');
        % 
        % 
        
        
        drawnow
        
        
        
    end
    
end

%%

senssel = readtable('processed/sens_sel.tsv','FileType','text','Delimiter','\t');

select = cells.sua.' & ntrials > 2*conf.mintrials & ncatch >= conf.mintrials;

sel = find(senssel.sens_sel & select');
length(sel)

thr = conf.sigthr * conf.fs_fire /  conf.statdownsample;

sum(pany(sel)>=thr)

%% results


% set a threshold on the minimum length of the significance
thr = conf.sigthr * conf.fs_fire /  conf.statdownsample;

% select units
% since we ttest against zero, we want twice the min number of trials
% we also want units to be SUA
select = cells.sua.' & ntrials > 2*conf.mintrials & ncatch >= nmincatch;%conf.mintrials;
fprintf('%d of %d with good behavior\n',sum(select),sum(cells.sua));


fprintf('\n\n---------------------------\n\tResults\n---------------------------\n');


% select all units
sel = find(select);


% get sensory-selective units by length
responsive = pany(sel)>=thr;

% count them
ncl = sum(responsive);
% quantile in permutated data
nclchance = quantile(sum(rndpany(sel,:) >=thr),0.95);
% non-parametric p-value
p = mean(sum(rndpany(sel,:) >= thr) >= ncl);
% output
fprintf('#cl detect: %d/%d (%.0f %%) (chance: %.0f, p = %.4f) thr=%.0f ms\n',ncl,length(sel),ncl/length(sel)*100,nclchance,p,conf.sigthr*1000);


% test difference stn vs. thal
sel_stn = find(strcmp(cells.anat.','stn') & select);
sel_thal = find(strncmp(cells.anat.','thal',1) & select);

x = nansum((pany(sel_stn)>=thr))./length(sel_stn);
y = nansum((pany(sel_thal)>=thr))./length(sel_thal);
rx = nansum((rndpany(sel_stn,:) >=thr))./length(sel_stn);
ry = nansum((rndpany(sel_thal,:) >=thr))./length(sel_thal);
ppermdif = mean(abs(rx - ry) >= abs(x - y));
fprintf('Difference stn (%.2f %%) - thal (%.2f %%): p = %.2f\n',100*x,100*y,ppermdif);


% get all responsive units
id =  sel(responsive);
n = length(sel(responsive));
tid1 = nan(1,n);
tid2 = nan(1,n);
pol = nan(1,n);

for i=1:n
    
    % get timings
    tim = timing(:,id(i));
    tim(isnan(tim)) = 0;
    
    % get significance start - stop indices
    tid1_ = find(tim ~= 0,1,'first');
    tid2_ = find(tim ~= 0,1,'last');
    
    if isempty(tid2_)
        tid2_ = length(tim);
    end
    % change to seconds
    tid1(i) = (tid1_-30)*0.01;
    tid2(i) = (tid2_-30)*0.01;
    
    pol(i) = mean(timing(tid1_:tid2_,id(i)));
end
bstrp1 = bootstrp(1000,@mean,tid1);
bstrp2 = bootstrp(1000,@mean,tid2-tid1);

fprintf('Onset: %.3f � %.3f\n',mean(tid1),std(bstrp1));
fprintf('Duration: %.3f � %.3f\n',mean(tid2-tid1),std(bstrp2));
fprintf('%d / %d positive\n',sum(pol>0),length(pol));


%% display units
cells(sel(responsive),:)
punit(sel(responsive))

% save selective and SUA
selective = false(size(cells.id));
selective(sel(responsive)) = 1;

seltbl = table(cells.id,selective,cells.sua,ntrials', ncatch','VariableNames',{'id','sens_sel','sua','ntrials','ncatch'});
writetable(seltbl,'processed/sensrev_sel.tsv','Delimiter','\t','FileType','text');


save('cntstim.mat','cntstim','tidx_stim_dwn');
