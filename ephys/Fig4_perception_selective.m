%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script generates the data and figures for perception-selective
% neurons (Figure 3)
clear;

% make it reproducible
rng('default');

% whether to plot results for each unit
doplot = 1;
% transparency of confidence intervals
alpha = [];

% add function directory
addpath ../functions/

%% General parameters
config_sua;

% time window for data analysis in samples(dont change)
win = -0.5*conf.fs_fire:1.5*conf.fs_fire;

% time indices in seconds
tidx = win./conf.fs_fire;

% time indices in seconds after downsampling
tidx_dwn = tidx(1:conf.statdownsample:end);

% min and max time for the plots
xl = [-0.3, 0.5];

% min and max time for statistics
statxl = [0, 0.4]; %0.4
statxl_baseline = [-0.3,0.0];

% select time for the clustering algorithm
statsel = find(tidx_dwn >= statxl(1) & tidx_dwn < statxl(2));
statsel_baseline = find(tidx_dwn >= statxl_baseline(1) & tidx_dwn < statxl_baseline(2));

timing = nan(length(statsel),100);

cnt = zeros(length(tidx_dwn),1);

%% load cell info

cells = readtable('../data/units.tsv','FileType','text','Delimiter','\t');
ncells = size(cells,1);

%% loop through cells
for cell = 1:ncells
    %%
    

    unitid = cells(cell,:).id{1};
    conf.subject = num2str(cells(cell,:).id{1}(1:end-6));
    conf.analyse_run = str2double(cells(cell,:).id{1}(end-4));
    fprintf(' -------------------------------------------\n');
    fprintf(' [ percept ] processing %s (%s)\n',unitid,cells(cell,:).anat{1});
    
    % load firing rates
    matfile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_stim.mat'];
    fprintf(' [ percept ] loading %s\n',matfile);
    dat = load(matfile,'raster','frate','behav','stat');
    behav = dat.behav;
    
    % remove segments marked as artifacts
    badseg_file = ['../data/' conf.subject '-' num2str(conf.analyse_run) '_bad.tsv'];
    behav = removeartefacts(behav,badseg_file,xl,'tonset');
    
    % resample so it takes less time to permute
    rate = resample(dat.frate,1,conf.statdownsample);
    
    % get indices and remove small clusters of perithr behavior with less
    % than two hits *and* misses
    idxperithr = strcmp(behav.type,'perithr');
    
    % remove catch trials
    bad = any(isnan(rate)) | behav.amp.'==0;% | mean(dat.frate(tsel,:))==0 ;
    fprintf(' [ percept ] removing %d bad/catch trials\n',sum(bad));
    behav(bad,:) = [];
    dat.frate(:,bad) = [];
    dat.raster(:,bad) = [];
    rate(:,bad) = [];
    idxperithr(bad) = [];
    
    % get indices for yes and no trials
    idxyes = strcmp(behav.resp,'yes') & behav.amp>0 & idxperithr;
    idxno = strcmp(behav.resp,'no') & behav.amp>0 & idxperithr;
    idxstim = behav.amp > 0;
    idxcatch = behav.amp==0;

    % save stimulus amplitude
    stimampyes(cell) = mean(behav.amp(idxyes));
    stimampno(cell) = mean(behav.amp(idxno));
    
    % save firing rate
    fryes(:,cell) = mean(dat.frate(:,idxyes),2);
    frno(:,cell) = mean(dat.frate(:,idxno),2);
    frstim(:,cell) = mean(dat.frate(:,idxyes | idxno),2);
    
    % save spectrum
    psdall(:,cell) = dat.stat.pw;
    
    % save min number of trials
    nperithr(cell) = min(sum(idxyes),sum(idxno));
    
    idsig = NaN;
    % check if sufficient data
    if nperithr(cell) >= conf.mintrials
        isperithr(cell) = 1;
        % cluster permutation hit vs. miss
        [punit(cell),pany(cell),rndpany(cell,:),idsig] = clustertest(rate(statsel,idxyes | idxno),idxyes(idxyes | idxno),conf.randperm);
        stimampval(cell) = ranksum(behav.amp(idxyes),behav.amp(idxno));
        if ~isnan(idsig)
            % if there is a significant cluster, record its latency
            timing(statsel(idsig),cell) = mean(rate(statsel(idsig),idxyes),2) - mean(rate(statsel(idsig),idxno),2);
        end
    else
        % if data insufficient, move on
        isperithr(cell) = 0;
        punit(cell) = NaN;
        pany(cell) = NaN;
        rndpany(cell,:) = NaN;
        stimampval(cell) = NaN;
        continue
    end
    
    idsigstim = NaN;
    ncatch(cell) = sum(idxcatch);
    % save min number of trials
    ntrials(cell) = sum(idxstim);
        

    load(['processed/stim/' unitid '_zbltrans.mat'],'zstim','zstim_bl','znst','znct');

    
    % check if sufficient data
    if znst >= 2*conf.mintrials && znct >= 10
        isok_stim(cell) = 1;
        % cluster permutation
        [punit_stim(cell),pany_stim(cell),rndpany_stim(cell,:),idsigstim] = clustertest_sign(zstim,zstim_bl,conf.randperm);
        % x = [stim_rate(statsel,:),mstim_rate(statsel,:)];
        % y = [0*stim_rate(1,:),0*mstim_rate(1,:)+1]==1;
        % [punit(cell),pany(cell),rndpany(cell,:),idsig] = clustertest(x,y,conf.randperm);
        
    else
        % if data insufficient, move on
        isokstim(cell) = 0;
        punit_stim(cell) = NaN;
        pany_stim(cell) = NaN;
        rndpany_stim(cell,:) = NaN;
    end

  

    %% PLOT
    if doplot
        close all
        
        %% 1) firing rate
        cur1 = figure; hold on;
        sel = find(tidx_dwn >= xl(1) & tidx_dwn <= xl(2));
        dosave = 0;
        if  isperithr(cell)
            % plot firing rate over time for hits and misses
            semshade_boot(rate(sel,idxyes).',alpha,conf.color.hit,tidx_dwn(sel),conf.randperm,'LineWidth',2);
            semshade_boot(rate(sel,idxno).',alpha,conf.color.miss,tidx_dwn(sel),conf.randperm,'LineWidth',2);
                    
            semshade_boot(rate(sel,idxstim).',alpha,'k',tidx_dwn(sel),conf.randperm,'LineWidth',2);

            % plot significance for sensory if any
            if ~all(isnan(idsigstim))
                sig = nan(size(tidx_dwn));
                sig(statsel(idsigstim)) = mean(frstim(statsel,cell));
                plot(tidx_dwn,sig,'LineWidth',5,'Color',conf.color.crej)

                if length(statsel(idsigstim)) >= 8 && cells(cell,:).sua
                    dosave = 1;
                end
            end

            % plot significance for perception if any
            if ~all(isnan(idsig))
                sig = nan(size(tidx_dwn));
                sig(statsel(idsig)) = mean(fryes(statsel,cell));
                plot(tidx_dwn,sig,'LineWidth',5,'Color',conf.color.miss)
                if length(statsel(idsig)) >= 8 && cells(cell,:).sua
                    dosave = 1;
                    cnt(statsel(idsig)) = cnt(statsel(idsig)) + 1;
                end
            end
            % make it nice
            xlim(xl);
            xlabel('Time from stimulus [s]');
            ylabel('Firing rate (spikes/s)');
            plot([0 0],ylim(),'k--');
            set(gcf,'Position',[50,500,300,400])
            title(unitid);
            
            %set(gca,'FontSize',18);
            set(gcf,'Position',[50,200,150,150])
            if dosave
                print(['figs/perc/' unitid '_fire.eps'],'-depsc');          
            end
        end

        %% plot  staircase
        cur2 = figure; hold on;
        % plot stimulus amplitude for yes and no
        idxyes2 = strcmp(behav.resp,'yes') & behav.amp>0;
        idxno2 = strcmp(behav.resp,'no') & behav.amp>0;
        
        plot(find(idxyes2),behav.amp(idxyes2),'go','LineWidth',2);
        plot(find(idxno2),behav.amp(idxno2),'ro','LineWidth',2);
        plot(find(idxyes2 & ~idxperithr),behav.amp(idxyes2 & ~idxperithr),'kx','LineWidth',2);
        plot(find(idxno2 & ~idxperithr),behav.amp(idxno2 & ~idxperithr),'kx','LineWidth',2);
        
        xlabel('Trials');
        ylabel('Amplitude');
        title('staircase');
        set(gcf,'Position',[50,400,150,150])
        if dosave
             saveas(cur2,['figs/perc/' unitid '_staircase.png']);
        end
        %% Raster plot
        if  isperithr(cell)
            cur3 = figure; hold on;
            % raster for hits and misses
            rasthit = dat.raster(:,idxyes);
            rastmiss = dat.raster(:,idxno);
            rast = [rastmiss 2*rasthit];
            raster(rast.',tidx,[conf.color.miss ; conf.color.hit] ,1);
            % make it nice
            xlim(xl);
            ylim([0,size(rast,2)]);
            plot([0 0],ylim(),'k--');
            title(unitid);
            xlabel('Time from stimulus [s]');
            ylabel('Trials');
            set(gcf,'Position',[50,600,150,150])
            cur4 = figure; hold on;
            if dosave
                saveas(cur3,['figs/' unitid '.png']);
            end
        end
        %%
        % sel = find(tidx_dwn >= xl(1) & tidx_dwn <= xl(2));
        % if  isperithr(cell)
        %     cur4 = figure; hold on;
        % 
        %     % plot firing rate over time for hits and misses
        %     plot(tidx_dwn(sel),mean(rate(sel,idxyes),2),'-','Color',conf.color.hit,'LineWidth',1.5);
        %     plot(tidx_dwn(sel),mean(rate(sel,idxno),2),'-','Color',conf.color.miss,'LineWidth',1.5);
        %     plot(tidx_dwn(sel),mean(rate(sel,idxstim),2),'Color','k','LineWidth',2);
        % 
        %     % plot significance if any
        %     ig = nan(1,size(dat.frate,1));
        %     if ~all(isnan(idsig))
        %         sig = nan(size(tidx_dwn));
        %         sig(statsel(idsig)) = mean(fryes(statsel,cell));
        %         plot(tidx_dwn,sig,'k','LineWidth',5)
        %     end
        %     % make it nice
        %     xlim(xl);
        %     set(gca,'XTickLabel',{});
        %     plot([0 0],ylim(),'k--');
        %     title(unitid);    
        %     %set(gca,'FontSize',18);
        %     set(gcf,'Position',[50,200,120,100])
        % 
        %     print(['figs/perc/sup_' unitid '_fire2.eps'],'-depsc');          
        % end
        % 
        commandwindow
        drawnow
    end
end


%% results

% set a threshold on the minimum length of the significance
thr = conf.sigthr * conf.fs_fire /  conf.statdownsample;

% select units
% since we ttest against zero, we want twice the min number of trials
% we also want units to be SUA
select = cells.sua.' & nperithr > conf.mintrials;
fprintf('%d of %d with good behavior\n',sum(select),sum(cells.sua));


fprintf('\n\n---------------------------\n\tResults\n---------------------------\n');


% select all units
sel = find(select);


% get sensory-selective units by length
responsive = pany(sel)>=thr;

% count them
ncl = sum(responsive);
% quantile in permutated data
nclchance = quantile(sum(rndpany(sel,:) >=thr),0.95);
% non-parametric p-value
p = mean(sum(rndpany(sel,:) >= thr) >= ncl);
% output
fprintf('#cl detect: %d/%d (%.0f %%) (chance: %.0f, p = %.4f) thr=%.0f ms\n',ncl,length(sel),ncl/length(sel)*100,nclchance,p,conf.sigthr*1000);


% test difference stn vs. thal
sel_stn = find(strcmp(cells.anat.','stn') & select);
sel_thal =find(strncmp(cells.anat.','thal',1) & select);

x = nansum((pany(sel_stn)>=thr))./length(sel_stn);
y = nansum((pany(sel_thal)>=thr))./length(sel_thal);
rx = nansum((rndpany(sel_stn,:) >=thr))./length(sel_stn);
ry = nansum((rndpany(sel_thal,:) >=thr))./length(sel_thal);
ppermdif = mean(abs(rx - ry) >= abs(x - y));
fprintf('Difference stn (%.2f %%) - thal (%.2f %%): p = %.2f\n',100*x,100*y,ppermdif);


% get all responsive units
id =  sel(responsive);
n = length(sel(responsive));
tid1 = nan(1,n);
tid2 = nan(1,n);
pol = nan(1,n);

for i=1:n
    
    % get timings
    tim = timing(:,id(i));
    tim(isnan(tim)) = 0;
    
    % get significance start - stop indices
    tid1_ = find(tim ~= 0,1,'first');
    tid2_ = find(tim ~= 0,1,'last');
    
    if isempty(tid2_)
        tid2_ = length(tim);
    end
    % change to seconds
    tid1(i) = (tid1_-50)*0.01;
    tid2(i) = (tid2_+1-50)*0.01;
    
    pol(i) = mean(timing(tid1_:tid2_,id(i)));
end
bstrp1 = bootstrp(1000,@mean,tid1);
bstrp2 = bootstrp(1000,@mean,tid2-tid1);

fprintf('Onset: %.2f � %.3f\n',mean(tid1),std(bstrp1));
fprintf('Duration: %.2f � %.3f\n',mean(tid2-tid1),std(bstrp2));
fprintf('%d / %d positive\n',sum(pol>0),length(pol));


%% display units
cells(sel(responsive),:)
punit(sel(responsive))


%%
selective = false(size(cells.id));
selective(sel(responsive)) = 1;

seltbl = table(cells.id,selective,'VariableNames',{'id','perc_sel'});
writetable(seltbl,'processed/perc_sel.tsv','Delimiter','\t','FileType','text');

save('cnt.mat','cnt','tidx_dwn');