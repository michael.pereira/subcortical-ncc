%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script generates the data and figures for cue- and task-selective
% neurons (Figure 2)

clear;

% make it reproducible
rng('default');

% whether to plot results for each unit
doplot = 1;
% transparency of confidence intervals
alpha = 0.4;

% add function directory
addpath ../functions/

%% General parameters
config_sua;

% time window for data analysis in samples(dont change)
win = -1*conf.fs_fire:4*conf.fs_fire;

% time indices in seconds
tidx = win./conf.fs_fire;

% time indices in seconds after downsampling
tidx_dwn = tidx(1:conf.statdownsample:end);

% min and max time for the plots
xl = [-0.5, 3.3];

% min and max time for statistics
% post-cue window
statxl(:,1) = [0.3, (0.3+0.5)]; % 300 ms for cue, then 500 ms
% stimulation window
statxl(:,2) = [0.3+0.5, (0.3+0.5+2)]; % 800 ms for cue and post-cue, then 2000 ms
% baseline before the cue
statxl_baseline = [-0.5, 0];

% select time for the clustering algorithm
statsel{1} = find(tidx >= statxl(1,1) & tidx < statxl(2,1));
statsel{2} = find(tidx >= statxl(1,2) & tidx < statxl(2,2));
statsel_baseline = find(tidx >= statxl_baseline(1) & tidx < statxl_baseline(2));


%% load cell info
cells = readtable('../data/units.tsv','FileType','text','Delimiter','\t');
ncells = size(cells,1);

%% loop through cells
for cell = 1:ncells
    
    unitid = cells(cell,:).id{1};
    conf.subject = num2str(cells(cell,:).id{1}(1:end-6));
    conf.analyse_run = str2double(cells(cell,:).id{1}(end-4));
    fprintf(' -------------------------------------------\n');
    fprintf(' [ task-selective ] processing %s (%s)\n',unitid,cells(cell,:).anat{1});
    
    %% load firing rate
    matfile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_cue.mat'];
    fprintf(' [ task-selective ] loading %s\n',matfile);
    dat = load(matfile,'raster','frate','behav','stat');
    
    % resample so it takes less time to permute
    rate = resample(dat.frate,1,conf.statdownsample);
    
    % remove segments marked as artifacts
    badseg_file = ['../data/' conf.subject '-' num2str(conf.analyse_run) '_bad.tsv'];
    [behav,badtbl] = removeartefacts(dat.behav,badseg_file,xl,'tcue');
    
    % get indices
    bad =  ~(strcmp(behav.type,'perithr') | strcmp(behav.type,'other'));% | behav.amp==0;% | mean(dat.frate(tsel,:))==0 ;
    fprintf(' [ task-selective ] removing %d bad trials\n',sum(bad));
    allbad(cell) = sum(bad);
    % remove bad trials
    behav(bad,:) = [];
    dat.frate(:,bad) = [];
    dat.raster(:,bad) = [];
    
    % get trial indices
    idxyes = strcmp(behav.resp,'yes');
    idxhit = strcmp(behav.resp,'yes') & behav.amp>0;% & idxperithr;
    idxmiss = strcmp(behav.resp,'no') & behav.amp>0;%& idxperithr;
    idxcrej = strcmp(behav.resp,'no') & behav.amp==0;% & idxperithr;
    idxfa = strcmp(behav.resp,'yes') & behav.amp==0;% & idxperithr;
    idxstim = behav.amp>0;
    idxcatch = behav.amp==0;
    
    allntrial(:,cell) = [sum(idxhit),sum(idxmiss),sum(idxcrej),sum(idxfa)];
    
    % baseline firing rate
    blr = dat.frate(statsel_baseline,:);
    % loop through post-cue and stimulation windows
    for j=1:2
        % get firing rate
        trr = dat.frate(statsel{j},:);
        % get difference in average firing rate
        dif = mean(blr,1)-mean(trr,1);
        
        % only do the permutation test if there's a sufficient N
        if length(dif) >= conf.mintrials
            % sign rank test
            [pbl(cell,j),~,st_] = signrank(dif,[],'method','approximate');
            zstat(cell,j) = st_.zval;
            % sign rank tests on permutated data
            for r=1:conf.randperm
                % permute sign of the difference
                rnddif = (2*round(rand(1,length(dif)))-1).*dif;
                [rndpbl(cell,r,j),~,rndst_] = signrank(rnddif,[],'method','approximate');
                rndzstat(cell,r,j) = rndst_.zval;
            end
        else
            pbl(cell,j) = NaN;
            zstat(cell,j) = NaN;
            rndpbl(cell,1:conf.randperm,j) = NaN;
            rndzstat(cell,1:conf.randperm,j) = NaN;
        end
    end
    
    if doplot
        %% Plot firing rate
        cur1 = figure(100); clf; hold on;
        % plot firing rate
        sel = find(tidx >= xl(1) & tidx <= xl(2));
        semshade_boot(dat.frate(sel,:).',alpha,[0 0 0],tidx(sel),conf.randperm,'LineWidth',2);
        xlim(xl);
        xlabel('Time from cue [s]');
        ylabel('Firing rate (spikes/s)');
        yl=ylim();
        plot([0.3,0.8],yl(1)*[1,1],'k-','LineWidth',4,'Color',0.7*[1,1,1]);
        plot([0.8,2.8],yl(1)*[1,1],'k-','LineWidth',4,'Color','k');
        % add '*' if significant
        if pbl(cell,1) < 0.05
            text(0.55,yl(2)-2,'*','Color',0.7*[1,1,1],'FontSize',24);
        end
        if pbl(cell,2) < 0.05
            text(0.8+1,yl(2)-2,'*','Color','k','FontSize',24);
        end
        fill([0,0.3,0.3,0,0],[yl(1),yl(1),yl(2),yl(2),yl(1)],0.8*[1,1,1],'LineStyle','none');
        plot([0,0],yl,'k-');
        plot([-0.5,0],yl(1)*[1,1],'k-');
        ylim(yl);
        title(unitid);
        set(gcf,'Position',[50,400,200,120])
        % print(['figs/prod/task/task_' unitid '_cue_frate.eps'],'-depsc');
        
        %% plot raster 
        cur2 = figure(200); clf; hold on;
        rasthit = dat.raster(:,idxhit);
        rastmiss = dat.raster(:,idxmiss);
        rastcrej = dat.raster(:,idxcrej);
        rastfa = dat.raster(:,idxfa);
        % order by stimulus onset
        [onsethit,reorderhit] = sort(behav.onset(idxhit));
        [onsetmiss,reordermiss] = sort(behav.onset(idxmiss));
        rast = [rasthit(:,reorderhit)  2*rastmiss(:,reordermiss) 3*rastcrej 4*rastfa];
        raster(rast.',tidx,[ conf.color.hit; conf.color.miss ; conf.color.crej ; conf.color.fa] ,1);
        hold on;
        plot(onsethit,1:sum(idxhit),'k--','LineWidth',1);
        plot(onsetmiss,sum(idxhit)+(1:sum(idxmiss)),'k--','LineWidth',1);
        xlim(xl);
        ylim([-2,size(rast,2)+2]);
        yl=ylim();
        plot([0,0],yl,'k-');
        plot([-0.5,0],[-1,-1],'k-');
        if pbl(cell,2) < 0.05
            plot([0.8,2.8],[-1,-1],'k-','LineWidth',4);
        else
            plot([0.8,2.8],[-1,-1],'k-');
        end
        title(unitid);
        xlabel('Time from stimulus [s]');
        ylabel('Trials');
        
        set(gcf,'Position',[50,600,200,200])
        %       saveas(cur2,['figs/prod/task/task_' unitid '_raster.png']);
        
        drawnow
        commandwindow
    end
end


%% Results

% j=1 -> cue-selective
% j=2 -> task-selective
clear cell
sel = cell(1,2);
selective = false(2,size(cells,1));

for j = 1:2
    
    % select units considered SUA with > 10 trials / condition
    sel{j} = sum(allntrial) >= conf.mintrials & cells.sua.';
    % number of selective units
    id = pbl(sel{j},j)<0.05;
    selective(j,id) = 1;
    
    n=sum(id);
    % number of selective units in permuted data
    nperm=sum(rndpbl(sel{j},:,j)<0.05 );
    % non-parametric p-value
    pperm = mean(nperm >= n);
    % output
    fprintf('%d) All (raster): %d/%d stim-sel (%.2f %%, p=%.5f %d decrease)\n',...
        j,n,sum(sel{j}),100*n/sum(sel{j}),pperm,sum(pbl(sel{j},j)<0.05 & zstat(sel{j},j) > 0));
    
    % same below
    
    % further select neurons in the STN
    sel_stn = find(strcmp(cells.anat.','stn') & sel{j});
    n_stn=sum(pbl(sel_stn,j)<0.05);
    nperm_stn=sum(rndpbl(sel_stn,:,j)<0.05);
    pperm_stn = mean(nperm_stn >= n_stn);
    fprintf('%d) STN (raster): %d/%d stim-sel (%.2f %%, p=%.5f) %d decrease\n',...
        j,n_stn,length(sel_stn),100*n_stn/length(sel_stn),pperm_stn,sum(pbl(sel_stn,j)<0.05 & zstat(sel_stn,j) > 0));
    
    % further select neurons in the thalamus
    sel_thal = find(strncmp(cells.anat.','thal',1) & sel{j});
    n_thal=sum(pbl(sel_thal,j)<0.05);
    nperm_thal=sum(rndpbl(sel_thal,:,j)<0.05);
    pperm_thal = mean(nperm_thal >= n_thal);
    fprintf('%d) Thal. (raster): %d/%d stim-sel (%.2f %%, p=%.5f %d decrease)\n',...
        j,n_thal,length(sel_thal),100*n_thal/length(sel_thal),pperm_thal,sum(pbl(sel_thal,j)<0.05 & zstat(sel_thal,j) > 0));
    
    % test on the difference on the ratio of selective units
    dif = (n_stn/length(sel_stn) - n_thal/length(sel_thal));
    permdif = (nperm_stn./length(sel_stn) - nperm_thal./length(sel_thal));
    pperm_dif = mean(abs(permdif) >= abs(dif));
    fprintf('Difference: p=%.5f\n',pperm_dif);
    
    % list selective units
    fprintf('\n\nSelective units: \n');
    cells.id(pbl(:,j).'<0.05 & sel{j})
end

%%

sel_int = sum(allntrial)>= conf.mintrials & cells.sua.';
n_int = sum(pbl(sel_int,1)<0.05 & pbl(sel_int,2)<0.05);
nperm_int = sum(rndpbl(sel_int,:,1)<0.05 & rndpbl(sel_int,:,2)<0.05);
pperm_int = mean(n_int < nperm_int);
fprintf('Intersection: %d/%d stim-sel (%.2f %%, p=%.5f; %d SUA)\n',...
    n_int,sum(pbl(sel_int,2)<0.05),100*n_int/sum(pbl(sel_int,2)<0.05),pperm_int, sum(pbl(sel_int,1)<0.05 & pbl(sel_int,2)<0.05));

% list intersection units
fprintf('\n\Intersection units: \n');
cells.id(sel_int' & pbl(:,1)<0.05 & pbl(:,2)<0.05 & cells.sua.'')

% save selective and SUA
seltbl = table(cells.id,selective(1,:).',selective(2,:).','VariableNames',{'id','cue_sel','task_sel'});
writetable(seltbl,'processed/cuetask_sel.tsv','Delimiter','\t','FileType','text');
