%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script generates the data and figures for the upper panels of Figure S4
clear;

% make it reproducible
rng('default');

% whether to plot results for each unit
doplot = 1;
% transparency of confidence intervals
alpha = 0.4;

% add function directory
addpath functions/


%% General parameters
config_sua;

% time window for data analysis in samples(dont change)
win = -0.5*conf.fs_fire:1.5*conf.fs_fire;

% time indices in seconds
tidx = win./conf.fs_fire;

% time indices in seconds after downsampling
tidx_dwn = tidx(1:conf.statdownsample:end);

% min and max time for the plots
xl = [-0.3, 0.5];

% min and max time for statistics
statxl = [0, 0.4]; %0.4

% select time for the clustering algorithm
statsel = find(tidx_dwn >= statxl(1) & tidx_dwn < statxl(2));


%% load cell info

cells = readtable('../data/units.tsv','FileType','text','Delimiter','\t');
ncells = size(cells,1);

%% loop through cells
for cell = 1:ncells
    
    unitid = cells(cell,:).id{1};
    conf.subject = num2str(cells(cell,:).id{1}(1:end-6));
    conf.analyse_run = str2double(cells(cell,:).id{1}(end-4));
    
    fprintf(' -------------------------------------------\n');
    fprintf(' [ percept ] processing %s (%s)\n',unitid,cells(cell,:).anat{1});
    
    %%
    
    matfile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_stim.mat'];
    fprintf(' [ percept ] loading %s\n',matfile);
    dat = load(matfile,'raster','frate','behav','stat');
    behav = dat.behav;
    
    % remove segments marked as artifacts
    badseg_file = ['../data/' conf.subject '-' num2str(conf.analyse_run) '_bad.tsv'];
    behav = removeartefacts(behav,badseg_file,xl,'tonset');
    
    % resample so it takes less time to permute
    rate = resample(dat.frate,1,conf.statdownsample);
    
    % get indices and remove small clusters of perithr behavior with less
    % than two hits *and* misses
    idxsel = strcmp(behav.type,'perithr');
    
    % remove catch trials
    bad = any(isnan(rate)) | behav.amp.'==0;% | mean(dat.frate(tsel,:))==0 ;
    fprintf(' [ control ] removing %d bad/catch trials\n',sum(bad));
    behav(bad,:) = [];
    
    dat.frate(:,bad) = [];
    dat.raster(:,bad) = [];
    rate(:,bad) = [];
    idxsel(bad) = [];
    
    % get indices
    med = median(behav.amp(idxsel));
    
    idxhigh = behav.amp>med & idxsel;
    idxlow = behav.amp<=med & idxsel;
    
    idxyes = strcmp(behav.resp,'yes') & behav.amp>0 & idxsel;
    idxno = strcmp(behav.resp,'no') & behav.amp>0 & idxsel;
    
    idxall = behav.amp > 0;
    
    % save stimulus amplitude
    stimamphigh(cell) = mean(behav.amp(idxhigh));
    stimamplow(cell) = mean(behav.amp(idxlow));
    
    % save firing rate
    frhigh(:,cell) = mean(dat.frate(:,idxhigh),2);
    frlow(:,cell) = mean(dat.frate(:,idxlow),2);
    frall(:,cell) = mean(dat.frate(:,idxhigh | idxlow),2);
    
    % save spectrum
    psdall(:,cell) = dat.stat.pw;
    
    % save min number of trials
    nperithr(cell) = min(sum(idxhigh),sum(idxlow));
    
    % check if sufficient data
    if min(sum(idxhigh),sum(idxlow)) >= conf.mintrials
        isperithr(cell) = 1;
        % cluster permutation hit vs. miss
        [punit(cell),pany(cell),rndpany(cell,:),idsig] = clustertest(rate(statsel,idxhigh | idxlow),idxhigh(idxhigh | idxlow),conf.randperm);
        stimampval(cell) = ranksum(behav.amp(idxhigh),behav.amp(idxlow));
    else
        isperithr(cell) = 0;
        punit(cell) = NaN;
        pany(cell) = NaN;
        rndpany(cell,:) = NaN;
        stimampval(cell) = NaN;
        continue
    end
    
    %% PLOT
    
    if doplot
        close all
        
        %% 1) firing rate
        cur = figure(100); clf; hold on;
        sel = find(tidx_dwn >= xl(1) & tidx_dwn <= xl(2));
        
        % plot firing rate over time for hits and misses
        semshade_boot(rate(sel,idxhigh).',alpha,conf.color.hit3,tidx_dwn(sel),conf.randperm,'LineWidth',2);
        semshade_boot(rate(sel,idxlow).',alpha,conf.color.hit1,tidx_dwn(sel),conf.randperm,'LineWidth',2);
        
        if isperithr(cell)
            % plot significance if any
            ig = nan(1,size(dat.frate,1));
            if ~all(isnan(idsig))
                sig = nan(size(tidx_dwn));
                sig(statsel(idsig)) = mean(frhigh(statsel,cell));
                plot(tidx_dwn,sig,'k','LineWidth',5)
            end
        end
        % make it nice
        xlim(xl);
        xlabel('Time from stimulus [s]');
        ylabel('Firing rate (spikes/s)');
        plot([0 0],ylim(),'k--');
        set(gcf,'Position',[50,500,300,400])
        title(unitid);
        set(gcf,'Position',[50,200,150,150])
        %print(['figs/prod/ctrl_amp/ctrl_' unitid '_amp.eps'],'-depsc');
        
        
        %% Raster plot
        cur2 = figure; hold on;
        if isperithr(cell)
            % raster for hits and misses
            rasthigh = dat.raster(:,idxhigh);
            rastlow = dat.raster(:,idxlow);
            rast = [rastlow 2*rasthigh];
            raster(rast.',tidx,[conf.color.hit1 ; conf.color.hit3] ,1);
        else
            % raster for all trials
            rast = dat.raster(:,idxall);
            raster(rast.',tidx,[0 0 0] ,1);
        end
        
        % make it nice
        xlim(xl);
        ylim([0,size(rast,2)]);
        plot([0 0],ylim(),'k--');
        title(unitid);
        xlabel('Time from stimulus [s]');
        ylabel('Trials');
        set(gcf,'Position',[50,600,150,150])
        %saveas(cur2,['figs/prod/ctrl_amp/ctrl_' unitid '_raster.png']);
        
        
        commandwindow
        drawnow
    end
end

%% results

% set a threshold on the minimum length of the significance
thr = conf.sigthr * conf.fs_fire /  conf.statdownsample;

% select units
% since we ttest against zero, we want twice the min number of trials
% we also want units to be SUA
select = cells.sua.' & nperithr > conf.mintrials;
fprintf('%d of %d with good behavior\n',sum(select),sum(cells.sua));
fprintf('\n\n---------------------------\n\tResults\n---------------------------\n');

% select all units
sel = find(select);

% get sensory-selective units by length
responsive = pany(sel)>=thr;

% count them
ncl = sum(responsive);
% quantile in permutated data
nclchance = quantile(sum(rndpany(sel,:) >=thr),0.95);
% non-parametric p-value
p = mean(sum(rndpany(sel,:) >= thr) >= ncl);
% output
fprintf('#cl detect: %d/%d (%.0f %%) (chance: %.0f, p = %.4f) thr=%.0f ms\n',ncl,length(sel),ncl/length(sel)*100,nclchance,p,conf.sigthr*1000);

% test difference stn vs. thal
sel_stn = find(strcmp(cells.anat.','stn') & select);
sel_thal =find(strncmp(cells.anat.','thal',1) & select);

x = nansum((pany(sel_stn)>=thr))./length(sel_stn);
y = nansum((pany(sel_thal)>=thr))./length(sel_thal);
rx = nansum((rndpany(sel_stn,:) >=thr))./length(sel_stn);
ry = nansum((rndpany(sel_thal,:) >=thr))./length(sel_thal);
ppermdif = mean(abs(rx - ry) >= abs(x - y));
fprintf('Difference stn (%.2f %%) - thal (%.2f %%): p = %.2f\n',100*x,100*y,ppermdif);

% get all responsive units
id =  sel(responsive);
n = length(sel(responsive));


%% display units
cells.id(sel(responsive)).'
%punit(sel(responsive))
