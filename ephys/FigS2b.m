%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================

% This script computes statistics for the spike stability over time

clear
if ~exist('processed/stability_fire.mat','file')
    fprintf('===================================\n');
    fprintf('=                                 =\n');
    fprintf('= You need to run compute_frate.m =\n');
    fprintf('=                                 =\n');
    fprintf('===================================\n');
    error('processed/stability_fire.mat not found');
end

config_sua;

cells = readtable('data/units.tsv','FileType','text','Delimiter','\t');
%load('processed/stability.mat');

k = linspace(0,100,600);
figure();

load('processed/stability_fire.mat');
subplot(211); hold on;
semshade_boot(subselfire_perc(:,cells.sua==1).'-1,[],'k',k,conf.randperm,'LineWidth',2)
grid on
ylim([-2,2])
ylabel('Change in firing rate (%)');
xlabel('Time of the recording  (% elapsed)');

