%========================================================
%
%   Pereira, Faivre, Bernasconi et al., 2023,
%   Subcortical correlates of consciousness with human single neuron recordings
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   14 Nov 2022
%
%========================================================



% !First script to run!
% This script processes raster data into firing rates

clear;

% whether to make plots of the spectrum
doplot = 1;

% get configuration
config_sua;

% add paths
addpath ../functions

allbehav = table();

% construct kernel for firing rate computation
L = conf.fs_fire/2+1;
t2 = (-L:L)./conf.fs_fire;
fwin = normpdf(t2,0,conf.kernelwidth);
fwin = fwin./sum(fwin);

target = {'stn','thal'};

cells = readtable('../data/units.tsv','FileType','text','Delimiter','\t');
ncells = size(cells,1);

oldsuj = '';
oldrun = -1;

% loop across each cell
for cell = 1:ncells
    % loop across cells
    conf.cellid = cells(cell,:).id{1}(end-3:end);
    conf.subject = num2str(cells(cell,:).id{1}(1:end-6));
    conf.analyse_run = str2double(cells(cell,:).id{1}(end-4));
    conf.target = cells(cell,:).anat{1};
    % get triggers
    trig = readtrig(conf);
    
    unitid = cells(cell,:).id{1};
    if cells(cell,:).sua == 1
        suatxt = 'SUA';
    else
        suatxt = 'multi unit';
    end
    fprintf(' [ processing ] %s (%s - %s)\n',unitid,conf.target,suatxt);
    
    % load firing rate and action potentials
    tblfire = readtable(sprintf('../data/%s_fire.tsv',unitid),'Delimiter','\t','FileType','text');
    
    %% compute firing rate with kernel
    % build sparce matrices with firing times
    x = zeros(1,ceil((max(trig.onset)+10)*conf.fs_fire));
    x(ceil(tblfire.spiketime*conf.fs_fire)) = 1;
    
    % apply kernel
    y = filter(fwin,1,x);
    y = y((L+1):end)*conf.fs_fire;
    
    %% spectral analysis
    
    % compute Welch spectrum
    [p,f] = pwelch(x,conf.fs_fire,conf.fs_fire/2,conf.fs_fire,conf.fs_fire);
    stat.pw = 10*log10(p);
    stat.f = f;
    pen = 0;
    % fit spectrum
    [ffitparam1, minerr1, ffit1, ffitnoise1, noise1, theta1,beta1] = psdfit(stat.pw,100,1,[0,10,1,0,0,20,1],[],[],20,pen);
    specfit(:,cell) = ffitparam1;
    ffit = ffit1;
    ffitnoise = ffitnoise1;
    noise = noise1;
    beta = beta1;
    
    if doplot
        %% plot spectral fit
        figure(1); clf; hold on;
        plot(stat.f,stat.pw,'Color',conf.color.thal,'LineWidth',2);
        plot(stat.f,ffit,'k-','LineWidth',2);
        plot(stat.f,ffitnoise,'k--','LineWidth',2);
        xlim([0,100]);
        yl=ylim();
        
        plot([1,1]*beta.freq,yl,'k');
        text(beta.freq+1,yl(2)-4,sprintf('%.1f Hz (%.1f dB)',beta.freq,beta.amp));
        
        
        set(gca,'XTick',0:10:100);
        xlabel('Frequency [Hz]');
        ylabel('Amplitude');
        title(unitid)
        set(gcf,'Position',[400,600,200,200])
        %        print(['figs/spectr/' unitid '_spectr.eps'],'-depsc');
        drawnow
    end
    
    %% Stimulus-locked epoching
    
    % get time
    time = linspace(0,length(y)/conf.fs_fire,length(y));
    
    % stim-locked
    ntr = length(trig.onset);
    win = conf.trial_stim(1)*conf.fs_fire:conf.trial_stim(2)*conf.fs_fire;
    tidx = win./conf.fs_fire;
    trfrate_stim = nan(length(win),ntr);
    trraster_stim = nan(length(win),ntr);
    for tr=1:ntr
        if ~isnan(trig.onset(tr))
            trfrate_stim(:,tr) = y(round(trig.onset(tr)*conf.fs_fire+win));
            trraster_stim(:,tr) = x(round(trig.onset(tr)*conf.fs_fire+win));
        end
    end
    
    %% Cue-locked epoching
    ntr = length(trig.cue_onset);
    win = conf.trial_cue(1)*conf.fs_fire:conf.trial_cue(2)*conf.fs_fire;
    trfrate_cue = nan(length(win),ntr);
    trraster_cue = nan(length(win),ntr);
    for tr=1:ntr
        if trig.cue_onset(tr) > 1
            trfrate_cue(:,tr) = y(round(trig.cue_onset(tr)*conf.fs_fire+win));
            trraster_cue(:,tr) = x(round(trig.cue_onset(tr)*conf.fs_fire+win));
        end
    end
    
    
    conf.trial_stim = [-0.5,1.5];
    win1 = tidx >= 0 & tidx < 0.2;
    win2 = tidx >= 0.2 & tidx < 0.4;
    
    % get behav
    resp = [trig.resp];
    confidence = [trig.conf];
    amp = [trig.stim_amp];
    amp(strcmp(trig.sdt,'fa')) = 0;
    amp(strcmp(trig.sdt,'cr')) = 0;
    
    % sel trial type
    typeperithr = strcmp(trig.trial_type,'perithr');
    typeother = strcmp(trig.trial_type,'other');
    
    %id = 1:length(amp);
    % get timings of first and last good trial firing sampling rate
    t1 = round(min(trig.cue_onset));
    t2 = round((max(trig.cue_onset)+conf.trlen));
    
    % reinterpolate firing rate from t1 to t2
    tsave = (t1*conf.fs_fire:t2*conf.fs_fire)/conf.fs_fire;
    newtime = linspace(t1,t2,600);
    nt = 1:600;
    subselfire(:,cell) = interp1(tsave,y(t1*conf.fs_fire:t2*conf.fs_fire),newtime);
    
    bfire(:,cell) = regress(subselfire(:,cell),[nt ; nt*0+1].');
    stabfire(cell) = bfire(1,cell)*600./bfire(2,cell);
    subselfire_perc(:,cell) = subselfire(:,cell)./bfire(2,cell);
    % reinterpolate mean spike amplitude from t1 to t2
    ssel = tblfire.spiketime >= t1 & tblfire.spiketime <= t2;
    
    % get inter-spike interval < 3 ms in percentage
    isi3 = 100*mean(diff(tblfire.spiketime)<0.003);
    
    % get indices and remove small clusters of perithr behavior with less
    % than two hits *and* misses
    trial_type = trig.trial_type;
    idxperithr_ = trig.stim_amp == 0 | strcmp(trial_type,'perithr');
    trial_type = repmat({'other'},size(trial_type));
    trial_type(1) = trig.trial_type(1);
    con = bwconncomp(idxperithr_.'==1);
    for c=1:length(con.PixelIdxList)
        nyes = sum(strcmp(resp(con.PixelIdxList{c}),'yes'));
        nno = sum(strcmp(resp(con.PixelIdxList{c}),'no'));
        if min(nyes,nno) >= 2
            trial_type(con.PixelIdxList{c}) = {'perithr'};
        end
    end
    
    % save behavior in a table for this session
    suj = repmat({conf.subject},size(resp));
    grp = repmat({conf.target},size(resp));
    ses = repmat({'ses-or'},size(resp));
    run = repmat(conf.analyse_run,size(resp));
    hitrate = trig.hitrate;
    hitrate(amp == 0) = -1;
    keep_ = min(sum(strcmp(resp,'yes') & amp>0),sum(strcmp(resp,'yes') & amp>0) > conf.mintrials);
    keep = repmat(keep_, size(resp));
    % table
    behav = table(suj,grp,ses,run,trial_type,amp,amp>0,trig.stim_onset,resp,confidence,trig.sdt,...
        hitrate, keep, trig.onset, trig.cue_onset, ...
        'VariableNames',...
        {'suj','grp','ses','run','type','amp','amp_gt0','onset','resp','conf','sdt','hitrate','keep','tonset','tcue'});
    stat.firestab = subselfire(:,cell);
    
    % make dir if it doesn't exist
    if ~exist('processed/','dir')
        mkdir('processed');
    end
    
    % save cue-locked epochs
    frate = trfrate_cue;
    raster = trraster_cue;
    savefile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_cue.mat'];
    fprintf(' [ saving ] %s\n',savefile);
    save(savefile,'frate','raster','behav','stat');
    
    % save stim-locked epochs
    frate = trfrate_stim;
    raster = trraster_stim;
    savefile = ['processed/' unitid '_fr' num2str(conf.kernelwidth) '_stim.mat'];
    fprintf(' [ saving ] %s\n',savefile);
    save(savefile,'frate','raster','behav','stat');
    
    % concat behavior if it is the first unit in that session
    if ~(strcmp(conf.subject,oldsuj) && conf.analyse_run == oldrun)
        allbehav = [allbehav ; behav];
    else
        % otherwise not, we don't want twice the same behavior if there are
        % > one neuron
        fprintf(' INFO: Skipping appending behavior\n(because it is not the first neuron of that session)\n');
    end
    
    oldsuj = conf.subject;
    oldrun = conf.analyse_run;
end


%% save
% stability
save('processed/stability_fire.mat','subselfire_perc','stabfire');%

% one big table for all behavior
allbehav.tonset = [];
allbehav.tcue = [];
allbehav.resp = strcmp(allbehav.resp,{'yes'});
allbehav.conf = strcmp(allbehav.conf,{'sure'});
writetable(allbehav,'processed/alldata.tsv','FileType','text','Delimiter','\t');

% stability and spectral fit parameters
newtbl = table(stabfire.'*100,specfit(1,:).',specfit(2,:).',specfit(3,:).',specfit(4,:).',specfit(5,:).',specfit(6,:).',specfit(7,:).',...
    'VariableNames',{'stabfire','specrot','specoff','specdec','speclin','specbetaamp','specbetafreq','specbetawidth'});
% add to cell table
newcells = [cells newtbl];
writetable(newcells,'processed/units_ext.tsv','FileType','text','Delimiter','\t');


