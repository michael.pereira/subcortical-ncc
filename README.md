# subcortical ncc R and MATLAB code

Pereira, Faivre, Bernasconi et al., 2023, Subcortical correlates of consciousness with human single neuron recordings.  

https://www.biorxiv.org/content/10.1101/2023.01.27.525684v1  
behav/ contains R script for behavior  
data/ contains .tsv data files   
ephys/ contains MATLAB scripts for ephys analyses  
functions/ contains MATLAB helper functions  
